(* ::Package:: *)

   (*COPY OF THE ORIGINAL PACKAGE*)
	(* FEB 2019 *)


(* ****************************************** *)
(* 

	Sept 2016
	M Jaber 
	
	== CDM AND DE PERTURBATIONS ==

	Starting from eqn 9.76 (Kolb) we write the system
	of coupled diff eqns for the evolution of
	\[Delta]c and \[Delta]x in terms of scale factor a(t)

	This package solves the evolution of both \[Delta]c and \[Delta]x given by:

	a^2 \[Delta]c''[a] + C1 \[Delta]c'[a] + C2 \[Delta]c[a] + C3 \[Delta]x[a] \[Equal] 0
	a^2 \[Delta]x''[a] + C1 \[Delta]x'[a] + C4 \[Delta]x[a] + C2 \[Delta]c[a] \[Equal] 0

	where coefficientes C1, C2, C3 and C4 are given by: 
	C1 = (3/2) a (1- wx \[CapitalOmega]x)
	C2 = -(3/2) \[CapitalOmega]c
	C3 = -(3/2) \[CapitalOmega]x 
	C4 = (cs2 k^2)/(a^2 H^2(a))+ C3
	
	with some initial conditions:
	
	\[Delta]cini
	\[Delta]xini
	\[Delta]cp
	\[Delta]xp
	aini

	and for given modes k


	It depends on the Packages: 
	
	- "NewCosmoUnits"
	- "DEnewEoS"
	- "DensityFunctionsDE"
`	- "SoundSpeedAndFourierModeDE"

>> febreuary 2017 <<
	MAJOR change: 
	linked to CosmoUnits package instead of PlanckCosmologyFullCMB
	to properly use natural units


>> november 2018

	NEWCOSMOUNITS to match the cosmology used for COLA and the thesis
	
 *)


(* ****************************************** *)


BeginPackage["SolveContrastNewtonwSEoS`"]

SolveContrastDEx::usage="SolveContrastDEx[$w0, $wi, $q, $zt, $kval] uses adiabatic sound of speed"
SolveContrastceff::usage="SolveContrastceff[$w0, $wi, $q, $zt, $kval, $ceff] ceff is a number \[NotEqual] adiabatic sound of speed"
$BACKGROUND::usage="$BACKGROUND set to False by Default. If set to True, \[Delta]x =0 and DE is only at Background level"

C1::usage="C1[$w0, $wi, $q, $zt, $a] coefficient for \[Delta]c' and \[Delta]x', C1 = (3/2) a (1- wx \[CapitalOmega]x)"
C2::usage="C2[$w0, $wi, $q, $zt, $a] coefficient of \[Delta]c, C2 = -(3/2) \[CapitalOmega]c"
C3::usage="C3[$w0, $wi, $q, $zt, $a] coefficient of \[Delta]x, C3 = -(3/2) \[CapitalOmega]x "
C4::usage="C4[$w0, $wi, $q, $zt, $k, $a] coefficient of \[Delta]x, C4 = (cs2 k^2)/(a^2 H^2(a))+ C3"
C4eff::usage="C4[$w0, $wi, $q, $zt, $k, $ceff, $a] coefficient of \[Delta]x, C4 = (cs2 k^2)/(a^2 H^2(a))+ C3 with cs2 = cad + ceff"


Begin["`Private`"]

(*Get["PlanckCosmologyFullCMB`"]; << february 2017 changed to CosmoUnits*)
Get["FidCosmoUnits`"]; (* nov 2018: test *)
Get["wSEoS`"];
Get["DensityFunctionswSEoS`"];
Get["SoundSpeedAndFourierModewSEoS`"];

(* default values for logical variables  *)
$BACKGROUND = False;
$USERAD = True;
(* 
	important! do not wwrite a:= something because that will cause to the function to be evaluated
	again and again each time it is called 

*)

	\[Delta]cini = 10^(-5);
	\[Delta]xini = 10^(-5);
	\[Delta]cp = 0;
	\[Delta]xp = 0;

(*  initial conditions for diff eqns*)
(* (b) we use this simplified initial conditions
	  to avoid exponential growth of \[Delta]c(a) even at background level and \[CapitalLambda]CDM
	*)
	(*
	\[Delta]cini = 10^(-5);
	\[Delta]xini = 10^(-5);
	\[Delta]cp = 0;
	\[Delta]xp = 0;
	*)

(*$aini := ak[$w0, $wi, $q, $zt, $kval];*) (* using k\[CenterDot]\[Tau] = 1 relationship *)
(*$aini = ain[$w0, $wi, $q, $zt, $kval]; *)(* using a\[CenterDot]H = k relationship *)
(*TODO: solve analytically for the value of $a0 for each model and substitute it here*)
aend = 1.4;




C1[$w0_, $wi_, $q_, $zt_, $a_]:=
	(3/2) $a (1-
		waT[$w0, $wi, $q, 1/(1+$zt), $a]OmegaDEaT[$w0, $wi, $q, 1/(1+$zt), $a]);


C2[$w0_, $wi_, $q_, $zt_, $a_]:= -(3/2) OmegaMaT[$w0, $wi, $q, 1/(1+$zt), $a];


C3[$w0_, $wi_, $q_, $zt_, $a_]:= -(3/2) OmegaDEaT[$w0, $wi, $q, 1/(1+$zt), $a];

C4[$w0_, $wi_, $q_, $zt_, $k_, $a_]:= Re[ $k^2 cad2a[$w0, $wi, $q, $zt, $a]/($a HubbleaT[$w0, $wi, $q, 1/(1+$zt), $a])^2 + C2[$w0, $wi, $q, $zt, $a]];

C4eff[$w0_, $wi_, $q_, $zt_, $k_, $ceff_, $a_]:= $k^2 (cad2a[$w0, $wi, $q, $zt, $a]+$ceff)/($a HubbleaT[$w0, $wi, $q, 1/(1+$zt), $a])^2 + C2[$w0, $wi, $q, $zt, $a];


SolveContrastDEx[$w0_, $wi_, $q_, $zt_, $kval_]:=
Module[{Dc, Dx, Dxback, $aini},
(* 

	important!!!
	do not wwrite the ":" in definition 
		example a:= something 
	because that will cause to the function to be evaluated
	again and again each time it is called 
	
*)

(*initial conditions*)
	(*$aini = ak[$w0, $wi, $q, $zt, $kval]; *)(* using a\[CenterDot]\[Tau] = k relationship *)
	
	$aini = ain[$w0, $wi, $q, $zt, $kval]; (* using a\[CenterDot]H = k relationship *)
	Print[$aini];

	
	(* 
	(TRUE INITIAL CONDITIONS)
	given the kval, the aini is calculated 
	and then the value for initial density contrasts

	$wini = waT[$w0, $wi, $q, 1/(1+$zt), $aini];
	\[Tau]ini = \[Tau]hor[$w0, $wi, $q, $zt, $aini];

	\[Delta]cini = 10^(-5);
	\[Delta]xini = (1+$wini)*\[Delta]cini;

	\[Delta]cp = -($kval^2)(\[Tau]ini/3)*\[Delta]cini;
	\[Delta]xp = -($kval^2)(\[Tau]ini/3)*\[Delta]xini;

	 *)
	
	
Dxback[$a_]:=0;

{Dc, Dx}=
	If[$w0==$wi==-1,
	
		{\[Delta]c, Dxback}/.NDSolve[]//First,
		If[TrueQ[$BACKGROUND], 
		{\[Delta]c, Dxback}/.NDSolve[
				{$a^2 \[Delta]c''[$a] + C1[$w0, $wi, $q, $zt, $a]\[Delta]c'[$a] + C2[$w0,$wi,$q,$zt,$a]\[Delta]c[$a]==0,
					\[Delta]c[$aini] == \[Delta]cini,
					\[Delta]c'[$aini] == \[Delta]cp},
					{\[Delta]c}, 
					{$a, $aini, aend}] //First,

		{\[Delta]c, \[Delta]x}/.NDSolve[
				{
				$a^2*\[Delta]c''[$a]+C1[$w0, $wi, $q, $zt, $a]*\[Delta]c'[$a]+C2[$w0, $wi, $q, $zt, $a]*\[Delta]c[$a]+C3[$w0, $wi, $q, $zt, $a]*\[Delta]x[$a]==0,
				$a^2*\[Delta]x''[$a]+C1[$w0, $wi, $q, $zt, $a]*\[Delta]x'[$a]+C4[$w0, $wi, $q, $zt, $kval, $a]*\[Delta]x[$a]+C2[$w0, $wi, $q, $zt, $a]*\[Delta]c[$a]==0,
				\[Delta]c[$aini]==\[Delta]cini,
				\[Delta]c'[$aini]==\[Delta]cp,
				\[Delta]x[$aini]==\[Delta]xini,
				\[Delta]x'[$aini]==\[Delta]xp},
				{\[Delta]c, \[Delta]x},
				{$a, $aini, aend}]//First];
				
Return[{Dc,Dx}];
	]
	

]




SolveContrastceff[$w0_, $wi_, $q_, $zt_, $kval_, $ceff_]:=
Module[{Dc,Dx, Dxback},

(* 
	important! do not wwrite a:= something because that will cause to the function to be evaluated
	again and again each time it is called 
*)
(*$aini = ak[$w0, $wi, $q, $zt, $kval];*)
$aini = ain[$w0, $wi, $q, $zt, $kval];
Dxback[$a_]:=0;

{Dc, Dx}=
	If[TrueQ[$BACKGROUND], 
		{\[Delta]c, Dxback}/.NDSolve[
				{$a^2 \[Delta]c''[$a] + C1[$w0, $wi, $q, $zt, $a]\[Delta]c'[$a] + C2[$w0,$wi,$q,$zt,$a]\[Delta]c[$a]==0,
					\[Delta]c[$aini]==\[Delta]cini,
					\[Delta]c'[$aini]==\[Delta]cp},
					{\[Delta]c}, {$a, $aini, aend}] //First,

		{\[Delta]c, \[Delta]x}/.NDSolve[
				{
				$a^2*\[Delta]c''[$a]+C1[$w0, $wi, $q, $zt, $a]*\[Delta]c'[$a]+C2[$w0, $wi, $q, $zt, $a]*\[Delta]c[$a]+C3[$w0, $wi, $q, $zt, $a]*\[Delta]x[$a]==0,
				$a^2*\[Delta]x''[$a]+C1[$w0, $wi, $q, $zt, $a]*\[Delta]x'[$a]+C4eff[$w0, $wi, $q, $zt, $kval, $ceff, $a]*\[Delta]x[$a]+C2[$w0, $wi, $q, $zt, $a]*\[Delta]c[$a]==0,
				\[Delta]c[$aini]==\[Delta]cini,
				\[Delta]c'[$aini]==\[Delta]cp,
				\[Delta]x[$aini]==\[Delta]xini,
				\[Delta]x'[$aini]==\[Delta]xp},
				{\[Delta]c, \[Delta]x},
				{$a, $aini, aend}]//First];
				
Return[{Dc,Dx}];

]



(*SolveContrastceff[$w0_, $wi_, $q_, $zt_, $kval_, $ceff_]:=
Module[{Dc,Dx, Dxback},
(* 
	important! do not wwrite a:= something because that will cause to the function to be evaluated
	again and again each time it is called 
*)
$aini = ak[$w0, $wi, $q, $zt, $kval];

Dxback[$a_]:=0;

{Dc, Dx}=
	If[TrueQ[$BACKGROUND], 
		{\[Delta]c, Dxback}/.NDSolve[
				{$a^2 \[Delta]c''[$a] + C1[$w0, $wi, $q, $zt, $a]\[Delta]c'[$a] + C2[$w0,$wi,$q,$zt,$a]\[Delta]c[$a]==0,
					\[Delta]c[$aini]==\[Delta]cini,
					\[Delta]c'[$aini]==\[Delta]cp},
					{\[Delta]c}, {$a, $aini, aend}] //First,

		{\[Delta]c, \[Delta]x}/.NDSolve[
				{
				$a^2*\[Delta]c''[$a]+C1[$w0, $wi, $q, $zt, $a]*\[Delta]c'[$a]+C2[$w0, $wi, $q, $zt, $a]*\[Delta]c[$a]+C3[$w0, $wi, $q, $zt, $a]*\[Delta]x[$a]==0,
				$a^2*\[Delta]x''[$a]+C1[$w0, $wi, $q, $zt, $a]*\[Delta]x'[$a]+C4eff[$w0, $wi, $q, $zt, $kval, $ceff, $a]*\[Delta]x[$a]+C2[$w0, $wi, $q, $zt, $a]*\[Delta]c[$a]==0,
				\[Delta]c[$aini]==\[Delta]cini,
				\[Delta]c'[$aini]==\[Delta]cp,
				\[Delta]x[$aini]==\[Delta]xini,
				\[Delta]x'[$aini]==\[Delta]xp},
				{\[Delta]c, \[Delta]x},
				{$a, $aini, aend}]//First];
				
Return[{Dc,Dx}];

]
*)





(*

SolveContrast[w0_,w1_,as_,ak_]:=

Module[{Dm,Dde,Dde\[CapitalLambda]},

Dde\[CapitalLambda][N_]:=0;

{Dm,Dde}=
If[TrueQ[$BACKGROUND],{\[Delta]m,Dde\[CapitalLambda]}/.NDSolve[{\[Delta]m''[$N]==A1[w0,w1,$N] \[Delta]m'[$N]+A2[w0,w1,$N] \[Delta]m[$N],
\[Delta]m[Ns]==10^-4,
\[Delta]m'[Ns]==0
},
{\[Delta]m},{$N,Ns,Nend}]//First,

{\[Delta]m,\[Delta]de}/.NDSolve[{\[Delta]m''[$N]==A1[w0,w1,$N] \[Delta]m'[$N]+A2[w0,w1,$N] \[Delta]m[$N]+A3[w0,w1,$N] \[Delta]de[$N],

\[Delta]de''[$N]==A1[w0,w1,$N] \[Delta]de'[$N]+A2[w0,w1,$N] \[Delta]m[$N]+A4[w0,w1,$N,ak] \[Delta]de[$N],

\[Delta]m[Ns]==\[Delta]mi,
\[Delta]m'[Ns]==0,
\[Delta]de[Ns]==\[Delta]mi,
\[Delta]de'[Ns]==0
},
{\[Delta]m,\[Delta]de},{$N,Ns,Nend}]//First];
Return[{Dm,Dde}];
]*)


(*

	In case BACKGROUND is set to FALSE this function should be use to calculate LCDM growth of density contrasts within the same Block/Module


*)

(*SolveContrast\[CapitalLambda][w0_,w1_,as_,ak_]:= 
Module[
{Dm\[CapitalLambda],Dde,Dde\[CapitalLambda]},

Dde\[CapitalLambda][N_]:=0;

{Dm\[CapitalLambda],Dde}={\[Delta]m,Dde\[CapitalLambda]}/.
		NDSolve[
		
	{
		\[Delta]m''[$N]==A1[w0,w1,$N] \[Delta]m'[$N]+A2[w0,w1,$N]\[Delta]m[$N],

		\[Delta]m[Ns]==\[Delta]mi,
		\[Delta]m'[Ns]==0
	},
		{\[Delta]m},
		{$N,Ns,Nend}
		
				]//First;

Return[{Dm\[CapitalLambda], Dde}];

]
*)


End[]

EndPackage[]
