(* ::Package:: *)

(* ****************************************** *)
(* 
	May 2018
	M Jaber 
	
	== DENSITY FUNCTIONS with new EoS ==

	This package contains the basic cosmological
	defitions and functions such as:
	
	- a0, N(a),

	- \[CapitalOmega](a) and \[CapitalOmega](N) for the different
	species, 

	It depends on the Package: 
	"NewCosmoUnits"  and
	"SEoS"
 *)


BeginPackage["DensityFunctionswSEoS`"]

$a0::usage="$a0"
$N::usage=""

$N0::usage="$N0 "
$a::usage="Scale factor as function of N"


$USERAD::usage="$USERAD Set to False by default"



(* as function of redshift z *)
OmegaRzT::usage="OmegaRzT[$w0, $w1,$q, $zt, $z]"
OmegaMzT::usage="OmegaMzT[$w0, $w1, $q, $zt, $z]"
OmegaDEzT::usage="OmegaDEzT[$w0, $w1, $q, $zt, $z]"
OmegaCzT::usage="OmegaCzT[$w0, $w1, $q, $zt, $z] "


OmegaRz\[CapitalLambda]::usage="OmegaRz\[CapitalLambda][$z]"
OmegaMz\[CapitalLambda]::usage="OmegaMz\[CapitalLambda][$z]"
OmegaDEz\[CapitalLambda]::usage="OmegaDEz\[CapitalLambda][$z]"
OmegaCz\[CapitalLambda]::usage="OmegaCz\[CapitalLambda][$z] "



(* as function of scale factor a(t) *)
OmegaRaT::usage="OmegaRaT[$w0, $w1,$q, $at, $a]"
OmegaMaT::usage="OmegaMaT[$w0, $w1, $q, $at, $a]"
OmegaDEaT::usage="OmegaDEaT[$w0, $w1, $q, $at, $a]"
OmegaCaT::usage="OmegaCaT[$w0, $w1, $q, $at, $a]"

OmegaRa\[CapitalLambda]::usage="OmegaRz\[CapitalLambda][$a]"
OmegaMa\[CapitalLambda]::usage="OmegaMz\[CapitalLambda][$a]"
OmegaDEa\[CapitalLambda]::usage="OmegaDEz\[CapitalLambda][$a]"
OmegaCa\[CapitalLambda]::usage="OmegaCz\[CapitalLambda][$a] "


(* as function of N = Ln(a/a0)*)
OmegaRNT::usage="OmegaRNT[$w0, $w1, $q, $Nt, $N]"
OmegaMNT::usage="OmegaMNT[$w0, $w1, $q, $Nt, $N]"
OmegaDENT::usage="OmegaDENT[$w0, $w1, $q, $Nt, $N]"
OmegaCNT::usage="OmegaCNT[$w0, $w1, $q, $Nt, $N]"

OmegaRN\[CapitalLambda]::usage="OmegaRNT[$N]"
OmegaMN\[CapitalLambda]::usage="OmegaMNT[$N]"
OmegaDEN\[CapitalLambda]::usage="OmegaDENT[$N]"
OmegaCN\[CapitalLambda]::usage="OmegaCNT[$N]"


Begin["`Private`"]

Get["FidCosmoUnits`"] 
Get["wSEoS`"] (* <<--- here are included the Hubble functions HubblezT, HubbleaT and HubbleNT *)


$a0 = 1;

$N0:=Log[$a0/$a0];
(*$a=$a0 Exp[$N];*)
$N = Log[$a/$a0];


$USERAD = True;
(*$COSMO = $Planck;*)
$USEPLANCK = True; (* feb 25th 2019*)


OmegaRzT[$w0_, $w1_, $q_, $zt_, $z_] := 
  If[TrueQ[$USERAD], 
		($\[Rho]r0 (1+$z)^4)/($\[Rho]r0 (1+$z)^4+ $\[Rho]m0 (1+$z)^3 + $\[Rho]de0 FzT[$w0, $w1, $q, $zt, $z]), 
	0];

OmegaMzT[$w0_, $w1_, $q_, $zt_, $z_] := $\[Rho]m0 (1+$z)^(3)/
    If[TrueQ[$USERAD], 
		($\[Rho]r0 (1+$z)^(4)+ $\[Rho]m0 (1+$z)^(3) + $\[Rho]de0 FzT[$w0, $w1, $q, $zt, $z]), 
		($\[Rho]m0 (1+$z)^(3) + $\[Rho]de0 FzT[$w0, $w1, $q, $zt, $z])];

OmegaDEzT[$w0_, $w1_,$q_, $zt_, $z_] := ($\[Rho]de0 FzT[$w0, $w1, $q, $zt, $z])/
   If[TrueQ[$USERAD], 
		($\[Rho]r0 (1+$z)^(4)+ $\[Rho]m0 (1+$z)^(3) + $\[Rho]de0 FzT[$w0, $w1, $q, $zt, $z]), 
		($\[Rho]m0 (1+$z)^(3) + $\[Rho]de0 FzT[$w0, $w1, $q, $zt, $z])];

OmegaCzT[$w0_, $w1_, $q_, $zt_, $z_]:=$\[Rho]c0 (1+$z)^(3)/
    If[TrueQ[$USERAD], 
		($\[Rho]r0 (1+$z)^(4)+ $\[Rho]m0 (1+$z)^(3) + $\[Rho]de0 FzT[$w0, $w1, $q, $zt, $z]), 
		($\[Rho]m0 (1+$z)^(3) + $\[Rho]de0 FzT[$w0, $w1, $q, $zt, $z])];


OmegaRz\[CapitalLambda][$z_] := 
  If[TrueQ[$USERAD], 
		($\[Rho]r0 (1+$z)^4)/($\[Rho]r0 (1+$z)^4+ $\[Rho]m0 (1+$z)^3 + $\[Rho]de0), 
	0];

OmegaMz\[CapitalLambda][$z_] := $\[Rho]m0 (1+$z)^(3)/
    If[TrueQ[$USERAD], 
		($\[Rho]r0 (1+$z)^(4)+ $\[Rho]m0 (1+$z)^(3) + $\[Rho]de0), 
		($\[Rho]m0 (1+$z)^(3) + $\[Rho]de0)];

OmegaDEz\[CapitalLambda][$z_] := ($\[Rho]de0)/
   If[TrueQ[$USERAD], 
		($\[Rho]r0 (1+$z)^(4)+ $\[Rho]m0 (1+$z)^(3) + $\[Rho]de0), 
		($\[Rho]m0 (1+$z)^(3) + $\[Rho]de0)];

OmegaCz\[CapitalLambda][$z_]:=$\[Rho]c0 (1+$z)^(3)/
    If[TrueQ[$USERAD], 
		($\[Rho]r0 (1+$z)^(4)+ $\[Rho]m0 (1+$z)^(3) + $\[Rho]de0), 
		($\[Rho]m0 (1+$z)^(3) + $\[Rho]de0)];



(*OmegaRaT[$w0_, $w1_, $q_, $at_, $a_] := 
  If[TrueQ[$USERAD], 
		($\[Rho]r0 $a^(-4))/($\[Rho]r0 $a^(-4)+ $\[Rho]m0 $a^(-3) + $\[Rho]de0 FaT[$w0, $w1, $q, $at, $a]), 
	0];

OmegaMaT[$w0_, $w1_, $q_, $at_, $a_] := $\[Rho]m0 $a^(-3)/
    If[TrueQ[$USERAD], 
		($\[Rho]r0 $a^(-4)+ $\[Rho]m0 $a^(-3) + $\[Rho]de0 FaT[$w0, $w1, $q, $at, $a]), 
		($\[Rho]m0 $a^(-3) + $\[Rho]de0 FaT[$w0, $w1, $q, $at, $a])];

OmegaDEaT[$w0_, $w1_,$q_, $at_, $a_] := ($\[Rho]de0 FaT[$w0, $w1, $q, $at, $a])/
   If[TrueQ[$USERAD], 
		($\[Rho]r0 $a^(-4)+ $\[Rho]m0 $a^(-3) + $\[Rho]de0 FaT[$w0, $w1, $q, $at, $a]), 
		($\[Rho]m0 $a^(-3) + $\[Rho]de0 FaT[$w0, $w1, $q, $at, $a])];

OmegaCaT[$w0_, $w1_, $q_, $at_, $a_]:= $\[Rho]c0 $a^(-3)/
    If[TrueQ[$USERAD], 
		($\[Rho]r0 $a^(-4)+ $\[Rho]m0 $a^(-3) + $\[Rho]de0 FaT[$w0, $w1, $q, $at, $a]), 
		($\[Rho]m0 $a^(-3) + $\[Rho]de0 FaT[$w0, $w1, $q, $at, $a])];*)

(*to avoid numerical errors when dealing with Subscript[\[Rho]\:2070, i]~10^106 we transform them into \[CapitalOmega]i*)

OmegaRaT[$w0_, $w1_, $q_, $at_, $a_] := 
  If[TrueQ[$USERAD], 
		($\[CapitalOmega]r0 $a^(-4))/($\[CapitalOmega]r0 $a^(-4)+ $\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0 FaT[$w0, $w1, $q, $at, $a]), 
	0];

OmegaMaT[$w0_, $w1_, $q_, $at_, $a_] := $\[CapitalOmega]m0 $a^(-3)/
    If[TrueQ[$USERAD], 
		($\[CapitalOmega]r0 $a^(-4)+ $\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0 FaT[$w0, $w1, $q, $at, $a]), 
		($\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0 FaT[$w0, $w1, $q, $at, $a])];

OmegaDEaT[$w0_, $w1_,$q_, $at_, $a_] := ($\[CapitalOmega]de0 FaT[$w0, $w1, $q, $at, $a])/
   If[TrueQ[$USERAD], 
		($\[CapitalOmega]r0 $a^(-4)+ $\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0 FaT[$w0, $w1, $q, $at, $a]),
		($\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0 FaT[$w0, $w1, $q, $at, $a])];

OmegaCaT[$w0_, $w1_, $q_, $at_, $a_]:= $\[CapitalOmega]c0 $a^(-3)/
    If[TrueQ[$USERAD], 
		($\[CapitalOmega]r0 $a^(-4)+ $\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0 FaT[$w0, $w1, $q, $at, $a]),
		($\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0 FaT[$w0, $w1, $q, $at, $a])];
		
		
		(****)
		
		
OmegaRa\[CapitalLambda][$a_] := 
  If[TrueQ[$USERAD], 
		($\[CapitalOmega]r0 $a^(-4))/($\[CapitalOmega]r0 $a^(-4)+ $\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0), 
	0];

OmegaMa\[CapitalLambda][$a_] := ($\[CapitalOmega]m0 $a^(-3))/
    If[TrueQ[$USERAD], 
		($\[CapitalOmega]r0 $a^(-4)+ $\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0), 
		($\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0)];

OmegaDEa\[CapitalLambda][$a_] := $\[CapitalOmega]de0/
   If[TrueQ[$USERAD], 
		($\[CapitalOmega]r0 $a^(-4)+ $\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0),
		($\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0)];

OmegaCa\[CapitalLambda][$a_]:= ($\[CapitalOmega]c0 $a^(-3))/
    If[TrueQ[$USERAD], 
		($\[CapitalOmega]r0 $a^(-4)+ $\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0),
		($\[CapitalOmega]m0 $a^(-3) + $\[CapitalOmega]de0)];


OmegaRNT[$w0_, $w1_, $q_, $Nt_, $N_] := 
	If[TrueQ[$USERAD], 
		($\[Rho]r0 ($a0 Exp[$N])^-4)/($\[Rho]r0 ($a0 Exp[$N])^(-4)+ $\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0 FNT[$w0, $w1, $q, $Nt, $N]),
		 0];

OmegaMNT[$w0_, $w1_, $q_, $Nt_, $N_] := $\[Rho]m0 ($a0 Exp[$N])^(-3)/
    If[TrueQ[$USERAD], 
		($\[Rho]r0 ($a0 Exp[$N])^(-4)+ $\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0 FNT[$w0, $w1, $q, $Nt, $N]), 
		($\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0 FNT[$w0, $w1, $q, $Nt, $N])];

OmegaDENT[$w0_, $w1_, $q_, $Nt_, $N_] := $\[Rho]de0 FNT[$w0, $w1, $q, $Nt, $N]/
    If[TrueQ[$USERAD], 
		($\[Rho]r0 ($a0 Exp[$N])^(-4)+ $\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0 FNT[$w0, $w1, $q, $Nt, $N]), 
		($\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0 FNT[$w0, $w1, $q, $Nt, $N])];

OmegaCNT[$w0_, $w1_, $q_, $Nt_, $N_] := $\[Rho]c0 ($a0 Exp[$N])^(-3)/
    If[TrueQ[$USERAD], 
		($\[Rho]r0 ($a0 Exp[$N])^(-4)+ $\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0 FNT[$w0, $w1, $q, $Nt, $N]),
		 ($\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0 FNT[$w0, $w1, $q, $Nt, $N])];


(****)


OmegaRN\[CapitalLambda][$N_] := 
	If[TrueQ[$USERAD], 
		($\[Rho]r0 ($a0 Exp[$N])^-4)/($\[Rho]r0 ($a0 Exp[$N])^(-4)+ $\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0),
		 0];

OmegaMN\[CapitalLambda][$N_] := ($\[Rho]m0 ($a0 Exp[$N])^(-3))/
    If[TrueQ[$USERAD], 
		($\[Rho]r0 ($a0 Exp[$N])^(-4)+ $\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0), 
		($\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0)];

OmegaDEN\[CapitalLambda][$N_] := $\[Rho]de0/
    If[TrueQ[$USERAD], 
		($\[Rho]r0 ($a0 Exp[$N])^(-4)+ $\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0 ), 
		($\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0)];

OmegaCN\[CapitalLambda][$N_]:= ($\[Rho]c0 ($a0 Exp[$N])^(-3))/
    If[TrueQ[$USERAD], 
		($\[Rho]r0 ($a0 Exp[$N])^(-4)+ $\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0 ),
		 ($\[Rho]m0 ($a0 Exp[$N])^(-3) + $\[Rho]de0)];


End[]

EndPackage[]
