(* ::Package:: *)

(* ****************************************** *)
(* 

	May 2018 
	M Jaber 
	
	== wSEoS ==

	This package introduces the following
	parametrization for DE EoS:
	
	w(z) = w0 + (w1-w0)(z/zT)^q/(1+(z/zT)^q)
	
	And it also calculates its integral

	F(z) = Exp[3 \!\(
\*SubsuperscriptBox[\(\[Integral]\), \(0\), \(z\)]\(\((1 + w\((z')\))\)/\((1 + z')\)\[DifferentialD]z'\)\)]

	It depends on the Packages: 

	- "PlanckCosmology"
	- "HubbleAndDensityFunctions"
		(because it contains $a0
			and $USERAD variables)
	
	>> August 2017 <<
	ADDITION: 
	H(z) for LCDM was explicitly included
	
	Linked to DensityFunctionsDE.m where
	the corresponding density functions for
	LCDM were included
		*)

(* ****************************************** *)


BeginPackage["wSEoS`"]

wzT::usage="wzT[$w0, $w1, $q, $zt, $z]"
waT::usage="waT[$w0, $w1, $q, $at, $a]"
wNT::usage="wNT[$w0, $w1, $q, $Nt, $N]"



FzT::usage="FzT[$w0, $w1, $q, $zt, $z] Integral of the EoS for zT parametrization"
FaT::usage="FaT[$w0, $w1, $q, $at, $a] Integral of the EoS for zT(aT) parametrization"
FNT::usage="FNT[$w0, $w1, $q, $Nt, $N] Integral of the EoS for zT(NT) parametrization"



Hubble\[CapitalLambda]z::usage="Hubble\[CapitalLambda]z[$z]"
Hubble\[CapitalLambda]a::usage="Hubble\[CapitalLambda]a[$a]"
Hubble\[CapitalLambda]N::usage="Hubble\[CapitalLambda]N[$N]"

HubblezT::usage="HubblezT[$w0, $w1, $q, $zt, $z]"
HubbleaT::usage="HubbleaT[$w0, $w1, $q, $at, $a]"
HubbleNT::usage="HubbleNT[$w0, $w1, $q, $Nt, $N]"



Begin["`Private`"]
Get["FidCosmoUnits`"]


$q=1; (*Global Value for $q*)
$USERAD=True;
$USEPLANCK = True; (*feb 25th 2019*)
(*$COSMO=$SEoS;*)
(*Instead of calling the HubbleAndDensityFunctions package, we simply define the variables:*)
$a0 = 1;
$N0:=Log[$a0/$a0];
$a=$a0 Exp[$N];


wzT[$w0_, $w1_, $q_, $zt_, $z_] :=
If[$zt == 0, $w1, $w0 + ($w1 - $w0)(($z/$zt)^$q/(1+($z/$zt)^$q))];

waT[$w0_, $w1_, $q_, $at_, $a_?NumericQ]:= 
If[$at==1, $w0, $w0 + ($w1 - $w0)(($at($a0-$a))^$q/(($a($a0-$at))^$q+($at($a0-$a))^$q))];

wNT[$w0_, $w1_, $q_, $Nt_, $N_?NumericQ]:=
If[$Nt == 0, $w0, $w0 + ($w1 - $w0)((Exp[-$N]-1)^$q/((Exp[-$Nt]-1)^$q+(Exp[-$N]-1)^$q))];




FzT[$w0_, $w1_, $q_, $zt_, $z_?NumericQ]:=
If[$w0==$w1==-1, 1,
Exp[3 NIntegrate[(1+wzT[$w0, $w1, $q, $zt, zp])/(1+zp), {zp, 0, $z}, AccuracyGoal->10]]
];

FaT[$w0_, $w1_, $q_, $at_, $a_?NumericQ]:=
Exp[-3 NIntegrate[(1+waT[$w0, $w1, $q, $at, ap])/ap, {ap, $a0, $a}, AccuracyGoal->10]];

FNT[$w0_, $w1_, $q_, $Nt_, $N_]:=
Exp[-3N]*Exp[-3 NIntegrate[wNT[$w0, $w1, $q, $Nt, Np], {Np, 0, $N}, AccuracyGoal->10]];


HubblezT[$w0_, $w1_, $q_, $zt_, $z_]:=
If[TrueQ[$USERAD], 
$H0 Sqrt[($\[CapitalOmega]r0 (1 + $z)^4 + $\[CapitalOmega]m0 (1 + $z)^3 + $\[CapitalOmega]de0 FzT[$w0, $w1, $q, $zt, $z])],
$H0 Sqrt[($\[CapitalOmega]m0 (1 + $z)^3 + $\[CapitalOmega]de0 FzT[$w0, $w1, $q, $zt, $z])]];

HubbleaT[$w0_, $w1_, $q_, $at_, $a_]:=   
If[TrueQ[$USERAD], 
$H0 \[Sqrt]($\[CapitalOmega]m0 ($a)^(-3)+ $\[CapitalOmega]r0 ($a)^(-4) + $\[CapitalOmega]de0 FaT[$w0, $w1, $q, $at, $a]), 
$H0 \[Sqrt]($\[CapitalOmega]m0 ($a)^(-3) + $\[CapitalOmega]de0 FaT[$w0, $w1, $q, $at, $a])];

HubbleNT[$w0_, $w1_, $q_, $Nt_, $N_]:=   
If[TrueQ[$USERAD], 
$H0 \[Sqrt]($\[CapitalOmega]m0 ($a0 Exp[$N])^(-3)+ $\[CapitalOmega]r0 ($a0 Exp[$N])^(-4) + $\[CapitalOmega]de0 FNT[$w0, $w1, $q, $Nt, $N]), 
$H0 \[Sqrt]($\[CapitalOmega]m0 ($a0 Exp[$N])^(-3) + $\[CapitalOmega]de0 FNT[$w0, $w1, $q, $Nt, $N])];




Hubble\[CapitalLambda]z[$z_]:=
If[TrueQ[$USERAD], 
$H0 Sqrt[($\[CapitalOmega]r0 (1 + $z)^4 + $\[CapitalOmega]m0 (1 + $z)^3 + $\[CapitalOmega]de0)],
$H0 Sqrt[($\[CapitalOmega]m0 (1 + $z)^3 + $\[CapitalOmega]de0)]];

Hubble\[CapitalLambda]a[$a_]:=   
If[TrueQ[$USERAD], 
$H0 \[Sqrt]($\[CapitalOmega]m0 ($a)^(-3)+ $\[CapitalOmega]r0 ($a)^(-4) + $\[CapitalOmega]de0), 
$H0 \[Sqrt]($\[CapitalOmega]m0 ($a)^(-3) + $\[CapitalOmega]de0)];

Hubble\[CapitalLambda]N[$N_]:=   
If[TrueQ[$USERAD], 
$H0 \[Sqrt]($\[CapitalOmega]m0 ($a0 Exp[$N])^(-3)+ $\[CapitalOmega]r0 ($a0 Exp[$N])^(-4) + $\[CapitalOmega]de0), 
$H0 \[Sqrt]($\[CapitalOmega]m0 ($a0 Exp[$N])^(-3) + $\[CapitalOmega]de0)];






End[]

EndPackage[]

