(* ::Package:: *)

(* ****************************************** *)
(* 
	May 2018
	M Jaber 
	
	== CDM PERTURBATIONS ==
	
	
	NO K-DEPENDENCE 
	
	
	This package solves the differential equations for
	CDM overdendities at 1st and 2nd order as function of
	scale factor as in L-PICOLA_cosmology paper:

	a^2 D1''[a] + (3/2)(1 - wx \[CapitalOmega]x)a D1'[a] - (3/2) \[CapitalOmega]m D1[a]  \[Equal] 0
	
	a^2 D2''[a] + (3/2)(1 - wx \[CapitalOmega]x)a D2'[a] - (3/2) \[CapitalOmega]m D2[a]  \[Equal] - (3/2)\[CapitalOmega]m D1^2[a]
	
	where 
	
	D1 is the first order solution and D2 the 2nd order solution.
	
	For the initial conditions we use
	
	aini = 10^-3
	\[Delta]cini = aini = 10^-3
	\[Delta]cp = 1
	
	
	It depends on the Packages: 
	
	- "NewCosmoUnits"
	- "SEoS"
	- "DensityFunctionsSEoS"

 *)
 


BeginPackage["COLAwSEoS`"]

(*functions to be exported to global context*)
SolveContrastD1::usage="SolveContrastD1[$w0, $wi, $q, $zt, $aini]"
SolveContrast2LPT::usage="SolveContrast2LPT[$w0, $wi, $q, $zt, $aini]"



Begin["`Private`"]


Get["FidCosmoUnits`"];
Get["wSEoS`"];
Get["DensityFunctionswSEoS`"];

$USERAD = True;
$USEPLANCK = True;
(*$COSMO = $Planck;*)


SolveContrastD1[$w0_, $wi_, $q_, $zt_, $aini_]:=

Module[{Dc},

\[Delta]ini = $aini; 
\[Delta]p = 1;

	
C1[$w0, $wi, $q, $zt, $a]:=
	(3/2) $a (1 -
	waT[$w0, $wi, $q, 1/(1+$zt), $a]OmegaDEaT[$w0, $wi, $q, 1/(1+$zt), $a]);

C2[$w0, $wi, $q, $zt, $a]:= -(3/2) OmegaMaT[$w0, $wi, $q, 1/(1+$zt), $a];

{Dc} = 

{\[Delta]c}/.NDSolve[
				{$a^2 \[Delta]c''[$a] + C1[$w0, $wi, $q, $zt, $a]\[Delta]c'[$a] + C2[$w0,$wi,$q,$zt,$a]\[Delta]c[$a]==0,
					\[Delta]c[$aini] == \[Delta]ini,
					\[Delta]c'[$aini] == \[Delta]p},
					{\[Delta]c}, 
					{$a, $aini, 1.2}] //First;

Return[{Dc}]

];
 



SolveContrast2LPT[$w0_, $wi_, $q_, $zt_, aini_] := Module[{D1, D2},


	
	
C1[$w0, $wi, $q, $zt, $a]:=
	(3/2) $a (1 -
	waT[$w0, $wi, $q, 1/(1+$zt), $a]OmegaDEaT[$w0, $wi, $q, 1/(1+$zt), $a]);

C2[$w0, $wi, $q, $zt, $a]:= -(3/2) OmegaMaT[$w0, $wi, $q, 1/(1+$zt), $a];



{D1} = {\[Delta]1}/.NDSolve[
				{$a^2 \[Delta]1''[$a] + C1[$w0, $wi, $q, $zt, $a]\[Delta]1'[$a] + C2[$w0,$wi,$q,$zt,$a]\[Delta]1[$a]==0,
					\[Delta]1[aini]==aini,
					\[Delta]1'[aini] == 1},
					{\[Delta]1}, 
					{$a, aini, 1.2}] //First;
					
{D2} = {\[Delta]2}/.NDSolve[
				{$a^2 \[Delta]2''[$a] + C1[$w0, $wi, $q, $zt, $a]\[Delta]2'[$a] + C2[$w0,$wi,$q,$zt,$a](\[Delta]2[$a] - (D1[$a])^2 ) == 0,
					\[Delta]2[aini] == aini*aini,
					\[Delta]2'[aini] == 2*aini},
					{\[Delta]2},
					{$a, aini, 1.2}]//First;

Return[{D1, D2}]

];



End[]

EndPackage[]
