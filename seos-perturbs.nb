(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     33887,        898]
NotebookOptionsPosition[     29676,        787]
NotebookOutlinePosition[     30033,        803]
CellTagsIndexPosition[     29990,        800]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["SEoS perturbations paper ", "Title",
 CellChangeTimes->{{3.7455877507875977`*^9, 3.7455877751836357`*^9}, {
   3.745587844794599*^9, 3.745587868609663*^9}, {3.760047823289731*^9, 
   3.760047831978733*^9}, {3.76005442974592*^9, 3.760054454666251*^9}, 
   3.760390565164124*^9},ExpressionUUID->"7f9cb2dd-5a08-48c1-ab40-\
46a7060cd80c"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"PrependTo", "[", 
     RowBox[{"$Path", ",", 
      RowBox[{"FileNameJoin", "[", 
       RowBox[{"{", 
        RowBox[{
        "$HomeDirectory", ",", "\"\<Dropbox\>\"", ",", " ", 
         "\"\<Programming\>\"", ",", " ", "\"\<CosmoPkgs\>\""}], "}"}], 
       "]"}]}], "]"}], ";"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"SetDirectory", "[", 
     RowBox[{"NotebookDirectory", "[", "]"}], "]"}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Get", "[", "\"\<FidCosmoUnits`\>\"", "]"}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Get", "[", "\"\<wSEoS`\>\"", "]"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Get", "[", "\"\<DensityFunctionswSEoS`\>\"", "]"}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Get", "[", "\"\<SoundSpeedAndFourierModewSEoS`\>\"", "]"}], 
    ";"}], "\[IndentingNewLine]", 
   RowBox[{"(*", 
    RowBox[{
     RowBox[{"Get", "[", "\"\<COLAwSEoS`\>\"", "]"}], ";"}], "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Get", "[", "\"\<SolveContrastNewtonwSEoS`\>\"", "]"}], ";"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 InitializationCell->True,
 CellChangeTimes->{{3.710687244330121*^9, 3.710687267442206*^9}, {
   3.710687337863954*^9, 3.710687338585905*^9}, {3.71078134424753*^9, 
   3.710781348538519*^9}, {3.710788700709607*^9, 3.710788702872484*^9}, 
   3.712426787737776*^9, 3.724702921755884*^9, 3.724704022209054*^9, {
   3.7356001334031267`*^9, 3.7356001474350843`*^9}, {3.7474175642817507`*^9, 
   3.747417578894331*^9}, {3.74741766004632*^9, 3.747417662085191*^9}, 
   3.760047849926412*^9, 3.760060503990129*^9, {3.76006265460822*^9, 
   3.760062668635495*^9}, {3.760076202344512*^9, 3.760076207371644*^9}, 
   3.760076245016724*^9, {3.760076769184649*^9, 3.760076770335245*^9}, 
   3.7601072963610783`*^9, {3.7601073659203663`*^9, 3.76010736958424*^9}, {
   3.760123131490176*^9, 3.76012313996763*^9}, {3.760390654954434*^9, 
   3.7603907139272747`*^9}, {3.760715318602022*^9, 
   3.760715350945394*^9}},ExpressionUUID->"59a089bf-cd60-48f9-b4b9-\
61e27bd5ca16"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"{", 
     RowBox[{
     "$w0loc", ",", "$wiloc", ",", "$qloc", ",", "$ztloc", ",", "$hloc", ",", 
      "$\[CapitalOmega]mloc"}], "}"}], "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"-", "0.92"}], ",", 
      RowBox[{"-", "0.999999"}], ",", "9.97", ",", "0.28", ",", "0.7322", ",",
       "0.3339"}], "}"}]}], ";"}], " "}]], "Input",
 InitializationCell->True,
 CellChangeTimes->{
  3.70751158827693*^9, 3.707511801706414*^9, 3.71002718200963*^9, {
   3.7474166459507837`*^9, 3.747416646364739*^9}, {3.7600480832290792`*^9, 
   3.760048083680479*^9}, {3.7602039959473543`*^9, 
   3.760203997144185*^9}},ExpressionUUID->"d086d603-598c-4a4a-87d9-\
68407a9d9d49"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"$ain", " ", "=", " ", 
   RowBox[{"10", "^", 
    RowBox[{"(", 
     RowBox[{"-", "3"}], ")"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"da", " ", "=", " ", "0.001"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"$aend", " ", "=", " ", 
   RowBox[{"1", "+", "da"}]}], ";"}]}], "Input",
 InitializationCell->True,
 CellChangeTimes->{{3.710096949783926*^9, 3.710096990824813*^9}, 
   3.7100970742461233`*^9, 
   3.710097138657482*^9},ExpressionUUID->"45176645-0b03-4662-abd4-\
bf05944962c5"],

Cell[CellGroupData[{

Cell["Choosing kpivote", "Subsubsection",
 CellChangeTimes->{{3.688914297987068*^9, 3.688914324585678*^9}, {
  3.688915498474374*^9, 
  3.6889154986228943`*^9}},ExpressionUUID->"25024269-79e1-48b0-801d-\
5b5106be5974"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "MODOD", " ", "QUE", " ", "ENTR\[CapitalOAcute]", " ", "EN", " ", "Z"}], 
    " ", "=", " ", "2"}], "*)"}], 
  RowBox[{"\.1c", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"k", "[", 
     RowBox[{
      RowBox[{"-", "1"}], ",", " ", 
      RowBox[{"-", "1"}], ",", " ", "1", ",", " ", "1", ",", " ", 
      RowBox[{"1", "/", 
       RowBox[{"(", 
        RowBox[{"1", "+", "1"}], ")"}]}]}], "]"}], "/", "$h"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.767732876438982*^9, 3.767732958794339*^9}, {
  3.767733008283976*^9, 3.767733015028675*^9}, {3.767733046213954*^9, 
  3.767733047380679*^9}},ExpressionUUID->"69cc7edb-cfef-487d-a844-\
1e75500aa1ea"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "MODOD", " ", "QUE", " ", "ENTR\[CapitalOAcute]", " ", "EN", " ", "Z"}], 
    " ", "=", " ", "0.28"}], "*)"}], 
  RowBox[{"\.1c", "\[IndentingNewLine]", 
   RowBox[{"k", "[", 
    RowBox[{"$w0loc", ",", "$wiloc", ",", "$qloc", ",", "$ztloc", ",", " ", 
     RowBox[{"1", "/", 
      RowBox[{"(", 
       RowBox[{"1", "+", "$ztloc"}], ")"}]}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.768911421953264*^9, 
  3.768911457179319*^9}},ExpressionUUID->"8924922d-8590-4401-939b-\
9ca29d4b7a03"],

Cell[BoxData["\.1c"], "Output",
 CellChangeTimes->{
  3.768911458614134*^9},ExpressionUUID->"251aabbf-7f9b-4850-8ab9-\
e7b46dface51"],

Cell[BoxData["0.00020718255443801973`"], "Output",
 CellChangeTimes->{
  3.768911458651635*^9},ExpressionUUID->"443b1752-905f-445b-8c62-\
f65c0ea48b94"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ScientificForm", "[", 
  RowBox[{"0.00020718255443801973`", "*", "$h"}], "]"}]], "Input",
 CellChangeTimes->{{3.7689117364067583`*^9, 3.768911748703806*^9}},
 NumberMarks->False,ExpressionUUID->"e1b39148-7a36-48f6-8cdb-13025c4000ac"],

Cell[BoxData[
 TagBox[
  InterpretationBox[
   RowBox[{"\<\"1.40345\"\>", "\[Times]", 
    SuperscriptBox["10", "\<\"-4\"\>"]}],
   0.00014034546237631456`,
   AutoDelete->True],
  ScientificForm]], "Output",
 CellChangeTimes->{
  3.76891146127138*^9, {3.76891173963973*^9, 
   3.768911749225319*^9}},ExpressionUUID->"6aa8a1d3-c84e-4953-af1b-\
f6ff8aadb03a"]
}, Open  ]],

Cell[BoxData["s"], "Input",
 CellChangeTimes->{
  3.768912137515883*^9},ExpressionUUID->"a81e7e1f-47bd-438e-b5b4-\
fb35fcf0c680"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"1", "/", 
   RowBox[{"ak", "[", 
    RowBox[{
     RowBox[{"-", "1"}], ",", " ", 
     RowBox[{"-", "1"}], ",", " ", "1", ",", " ", "1", ",", 
     RowBox[{"0.0005", "*", "$h"}]}], "]"}]}], "-", "1"}]], "Input",
 CellChangeTimes->{{3.767733065198305*^9, 3.767733096957593*^9}, {
  3.767733131402298*^9, 3.76773319940149*^9}, {3.767733318752239*^9, 
  3.767733352456295*^9}},ExpressionUUID->"e5d7790c-c343-4b68-9797-\
09fc6daa62a2"],

Cell[BoxData["22.424729379557483`"], "Output",
 CellChangeTimes->{
  3.7677329209311447`*^9, 3.767732959319317*^9, {3.7677329979147997`*^9, 
   3.767733016715535*^9}, {3.76773315847383*^9, 3.767733203707261*^9}, 
   3.767733325616733*^9, 
   3.767733356156846*^9},ExpressionUUID->"ee3e47b6-8a72-4a8e-b47c-\
eec1d4a33431"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ScientificForm", "[", ".0005", "]"}]], "Input",
 CellChangeTimes->{3.767733215117751*^9},
 NumberMarks->False,ExpressionUUID->"a5c408fe-c12d-4415-ac65-5046689f2182"],

Cell[BoxData[
 TagBox[
  InterpretationBox[
   RowBox[{"\<\"5.\"\>", "\[Times]", 
    SuperscriptBox["10", "\<\"-4\"\>"]}],
   0.0005,
   AutoDelete->True],
  ScientificForm]], "Output",
 CellChangeTimes->{3.767733020705418*^9, 
  3.767733215538883*^9},ExpressionUUID->"2c9de5b6-6104-44d7-8107-\
137bb343ffcd"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ScientificForm", "[", "0.0002006715694161095`", "]"}]], "Input",
 NumberMarks->False,ExpressionUUID->"c1c9da39-9254-4d32-8adf-f034859fa6e2"],

Cell[BoxData[
 TagBox[
  InterpretationBox[
   RowBox[{"\<\"2.00672\"\>", "\[Times]", 
    SuperscriptBox["10", "\<\"-4\"\>"]}],
   0.0002006715694161095,
   AutoDelete->True],
  ScientificForm]], "Output",
 CellChangeTimes->{
  3.767733000187139*^9},ExpressionUUID->"8b8a965e-247e-46af-aa1b-\
fe086459f529"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"k", "[", 
    RowBox[{
     RowBox[{"-", "1"}], ",", " ", 
     RowBox[{"-", "1"}], ",", " ", "1", ",", " ", "1", ",", " ", "0.0003"}], 
    "]"}], "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.7663541624694777`*^9, 3.766354208248151*^9}, 
   3.767732874505931*^9},ExpressionUUID->"169e8819-838d-43ba-b405-\
1a295c2b931b"],

Cell[BoxData["0.011995649923223429`"], "Output",
 CellChangeTimes->{{3.766354181771905*^9, 
  3.766354208600697*^9}},ExpressionUUID->"53cf257f-8d8f-49d4-b9fc-\
c5f0ac41145f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"k", "[", 
   RowBox[{
    RowBox[{"-", "1"}], ",", " ", 
    RowBox[{"-", "1"}], ",", " ", "1", ",", " ", "1", ",", " ", "0.000027"}], 
   "]"}], "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.766354237408738*^9, 
  3.7663542585850058`*^9}},ExpressionUUID->"27b4f0f5-5e49-45bc-952c-\
e07e4057f3a8"],

Cell[BoxData["0.10902136147851896`"], "Output",
 CellChangeTimes->{{3.766354241076495*^9, 
  3.7663542588920803`*^9}},ExpressionUUID->"41186dc6-a7d0-49a9-91dc-\
9441ec30b463"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"(*", " ", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"at", " ", "z"}], " ", "=", " ", 
    RowBox[{
     RowBox[{
      RowBox[{"zT", " ", "--"}], 
      RowBox[{"->", " ", 
       RowBox[{"k", " ", "value", " ", "using", " ", "the", " ", 
        RowBox[{"aT", "\[CenterDot]", "H"}], 
        RowBox[{"(", "aT", ")"}]}]}]}], " ", "=", " ", 
     RowBox[{"k", " ", "relationship"}]}]}], " ", "\[IndentingNewLine]", 
   "*)"}], " ", "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"k\[CapitalLambda]01", "=", 
     RowBox[{"k", "[", 
      RowBox[{
       RowBox[{"-", "1"}], ",", 
       RowBox[{"-", "1"}], ",", "1", ",", " ", "1", ",", 
       RowBox[{"1", "/", 
        RowBox[{"(", 
         RowBox[{"1", "+", "3"}], ")"}]}]}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"kq01", "=", 
     RowBox[{"k", "[", 
      RowBox[{"$w0loc", ",", "$wiloc", ",", "$qloc", ",", "$ztloc", ",", 
       RowBox[{"1", "/", 
        RowBox[{"(", 
         RowBox[{"1", "+", "3"}], ")"}]}]}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"ScientificForm", "[", "k\[CapitalLambda]01", "]"}], ",", 
       RowBox[{"ScientificForm", "[", "kq01", "]"}]}], "}"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"ain", "[", 
        RowBox[{
         RowBox[{"-", "1"}], ",", 
         RowBox[{"-", "1"}], ",", "1", ",", " ", "1", ",", 
         "k\[CapitalLambda]01"}], "]"}], ",", 
       RowBox[{"ain", "[", 
        RowBox[{
        "$w0loc", ",", "$wiloc", ",", "$qloc", ",", "$ztloc", ",", "kq01"}], 
        "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"ak", "[", 
        RowBox[{
         RowBox[{"-", "1"}], ",", 
         RowBox[{"-", "1"}], ",", "1", ",", " ", "1", ",", 
         "k\[CapitalLambda]01"}], "]"}], ",", 
       RowBox[{"ak", "[", 
        RowBox[{
        "$w0loc", ",", "$wiloc", ",", "$qloc", ",", "$ztloc", ",", "kq01"}], 
        "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"1", "/", 
        RowBox[{"ain", "[", 
         RowBox[{
          RowBox[{"-", "1"}], ",", 
          RowBox[{"-", "1"}], ",", "1", ",", " ", "1", ",", 
          "k\[CapitalLambda]01"}], "]"}]}], "-", "1"}], "}"}]}], 
    "}"}]}]}]], "Input",
 CellChangeTimes->{{3.688914282804867*^9, 3.68891428387757*^9}, {
   3.751644911367774*^9, 3.751644911376745*^9}, {3.760715376031633*^9, 
   3.7607153952232237`*^9}, 3.7638386953859386`*^9, {3.7638408271096697`*^9, 
   3.763840848114148*^9}, {3.766354136776431*^9, 3.766354160330007*^9}, {
   3.766354221930401*^9, 
   3.766354233726877*^9}},ExpressionUUID->"f11d361e-4ee9-4ea0-9966-\
fed98a311f6b"],

Cell[BoxData["0.011995649923223429`"], "Output",
 CellChangeTimes->{3.763840787065873*^9, 3.763840819640945*^9, 
  3.7638408598937817`*^9, 
  3.766354228205531*^9},ExpressionUUID->"baff5b36-d2a0-4fec-9045-\
8ffb147ffc56"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     TagBox[
      InterpretationBox[
       RowBox[{"\<\"2.55224\"\>", "\[Times]", 
        SuperscriptBox["10", "\<\"-4\"\>"]}],
       0.00025522363542378743`,
       AutoDelete->True],
      ScientificForm], ",", 
     TagBox[
      InterpretationBox[
       RowBox[{"\<\"2.55491\"\>", "\[Times]", 
        SuperscriptBox["10", "\<\"-4\"\>"]}],
       0.0002554908028968475,
       AutoDelete->True],
      ScientificForm]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0.25`", ",", "0.2500000000000001`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0.07150969967390274`", ",", "0.07137243424943704`"}], "}"}], ",", 
   RowBox[{"{", "3.`", "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.763840787065873*^9, 3.763840819640945*^9, 
  3.7638408598937817`*^9, 
  3.766354239104364*^9},ExpressionUUID->"f7838265-fd42-4bac-92ca-\
ec24f4d1c5e6"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"at", " ", "z"}], " ", "=", " ", 
    RowBox[{
     RowBox[{
      RowBox[{"2", " ", "--"}], 
      RowBox[{"->", " ", 
       RowBox[{"k", " ", "value", " ", "using", " ", "the", " ", 
        RowBox[{"a", "\[CenterDot]", "H"}]}]}]}], " ", "=", " ", 
     RowBox[{"k", " ", "relationship"}]}]}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"k\[CapitalLambda]02", "=", 
     RowBox[{"k", "[", 
      RowBox[{
       RowBox[{"-", "1"}], ",", 
       RowBox[{"-", "1"}], ",", "1", ",", " ", "1", ",", 
       RowBox[{"1", "/", 
        RowBox[{"(", 
         RowBox[{"1", "+", "2"}], ")"}]}]}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"kq02", "=", 
     RowBox[{"k", "[", 
      RowBox[{"$w0loc", ",", "$wiloc", ",", "$qloc", ",", "$ztloc", ",", 
       RowBox[{"1", "/", 
        RowBox[{"(", 
         RowBox[{"1", "+", "2"}], ")"}]}]}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"ScientificForm", "[", "k\[CapitalLambda]02", "]"}], ",", 
       RowBox[{"ScientificForm", "[", "kq02", "]"}]}], "}"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"ain", "[", 
        RowBox[{
         RowBox[{"-", "1"}], ",", 
         RowBox[{"-", "1"}], ",", "1", ",", " ", "1", ",", 
         "k\[CapitalLambda]02"}], "]"}], ",", 
       RowBox[{"ain", "[", 
        RowBox[{
        "$w0loc", ",", "$wiloc", ",", "$qloc", ",", "$ztloc", ",", 
         "k\[CapitalLambda]02"}], "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
     
     RowBox[{"{", 
      RowBox[{
       RowBox[{"ak", "[", 
        RowBox[{
         RowBox[{"-", "1"}], ",", 
         RowBox[{"-", "1"}], ",", "1", ",", " ", "1", ",", 
         "k\[CapitalLambda]02"}], "]"}], ",", 
       RowBox[{"ak", "[", 
        RowBox[{
        "$w0loc", ",", "$wiloc", ",", "$qloc", ",", "$ztloc", ",", 
         "k\[CapitalLambda]02"}], "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
     
     RowBox[{"{", 
      RowBox[{
       RowBox[{"1", "/", 
        RowBox[{"ain", "[", 
         RowBox[{
          RowBox[{"-", "1"}], ",", 
          RowBox[{"-", "1"}], ",", "1", ",", " ", "1", ",", 
          "k\[CapitalLambda]02"}], "]"}]}], "-", "1"}], "}"}]}], "}"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.683161425037785*^9, 3.683161425946073*^9}, {
   3.683162951133165*^9, 3.683162956011346*^9}, 3.683162999848049*^9, 
   3.683163077727189*^9, {3.68330527817782*^9, 3.6833053526822033`*^9}, {
   3.683307417657935*^9, 3.683307419645567*^9}, {3.683309851010384*^9, 
   3.6833098686696873`*^9}, {3.683320599338426*^9, 3.6833206084732513`*^9}, {
   3.683379408438733*^9, 3.683379414057177*^9}, {3.683379447410449*^9, 
   3.68337946258189*^9}, {3.683382255146508*^9, 3.683382255944065*^9}, {
   3.683382417349123*^9, 3.683382436639237*^9}, {3.6833825355828323`*^9, 
   3.683382538265565*^9}, {3.683384645485897*^9, 3.6833846567234173`*^9}, {
   3.683397149988208*^9, 3.683397152299148*^9}, {3.683399810205421*^9, 
   3.6833998145005293`*^9}, {3.683399849396399*^9, 3.683399888873996*^9}, 
   3.6834032297574997`*^9, {3.683403948060717*^9, 3.683403948896758*^9}, {
   3.68340409806301*^9, 3.6834040995667048`*^9}, {3.68340450910546*^9, 
   3.68340450985048*^9}, {3.688411129470613*^9, 3.688411129843931*^9}, {
   3.688413889116188*^9, 3.6884138904826183`*^9}, 3.688414162510841*^9, {
   3.688414205792842*^9, 3.6884142215387707`*^9}, {3.688416584518701*^9, 
   3.688416630556674*^9}, {3.6884166683048897`*^9, 3.6884166853654623`*^9}, {
   3.68841672761899*^9, 3.688416779374528*^9}, {3.68841681903526*^9, 
   3.688416851606502*^9}, {3.688416903079126*^9, 3.688416945839342*^9}, {
   3.688418702069017*^9, 3.688418753226075*^9}, {3.688418786160646*^9, 
   3.6884188187732983`*^9}, {3.688418852498538*^9, 3.688418909037262*^9}, {
   3.6884189660281343`*^9, 3.6884190044603233`*^9}, {3.688420825714984*^9, 
   3.688420871764834*^9}, {3.688421309144226*^9, 3.6884213173229427`*^9}, {
   3.688912222313799*^9, 3.68891223061893*^9}, {3.688912293117421*^9, 
   3.688912323392864*^9}, {3.6889123650271673`*^9, 3.6889123672072697`*^9}, {
   3.6889125438175917`*^9, 3.688912544111505*^9}, {3.688912639820519*^9, 
   3.688912644515976*^9}, {3.68891277628225*^9, 3.68891281791791*^9}, {
   3.763840988224806*^9, 
   3.7638409998346233`*^9}},ExpressionUUID->"58614303-03a6-4d8e-9f10-\
bb283a27df38"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"at", " ", "z"}], " ", "=", " ", 
    RowBox[{
     RowBox[{
      RowBox[{"4", " ", "--"}], 
      RowBox[{"->", " ", 
       RowBox[{"k", " ", "value", " ", "using", " ", "the", " ", 
        RowBox[{"a", "\[CenterDot]", "H"}]}]}]}], " ", "=", " ", 
     RowBox[{"k", " ", "relationship"}]}]}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"k\[CapitalLambda]04", "=", 
     RowBox[{"k", "[", 
      RowBox[{
       RowBox[{"-", "1"}], ",", 
       RowBox[{"-", "1"}], ",", "1", ",", " ", "1", ",", 
       RowBox[{"1", "/", 
        RowBox[{"(", 
         RowBox[{"1", "+", "4"}], ")"}]}]}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"kq04", "=", 
     RowBox[{"k", "[", 
      RowBox[{"$w0loc", ",", "$wiloc", ",", "$qloc", ",", "$ztloc", ",", 
       RowBox[{"1", "/", 
        RowBox[{"(", 
         RowBox[{"1", "+", "4"}], ")"}]}]}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"ScientificForm", "[", "k\[CapitalLambda]04", "]"}], ",", 
       RowBox[{"ScientificForm", "[", "kq04", "]"}]}], "}"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"ain", "[", 
        RowBox[{
         RowBox[{"-", "1"}], ",", 
         RowBox[{"-", "1"}], ",", "1", ",", " ", "1", ",", 
         "k\[CapitalLambda]04"}], "]"}], ",", 
       RowBox[{"ain", "[", 
        RowBox[{
        "$w0loc", ",", "$wiloc", ",", "$qloc", ",", "$ztloc", ",", 
         "k\[CapitalLambda]04"}], "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
     
     RowBox[{"{", 
      RowBox[{
       RowBox[{"ak", "[", 
        RowBox[{
         RowBox[{"-", "1"}], ",", 
         RowBox[{"-", "1"}], ",", "1", ",", " ", "1", ",", 
         "k\[CapitalLambda]04"}], "]"}], ",", 
       RowBox[{"ak", "[", 
        RowBox[{
        "$w0loc", ",", "$wiloc", ",", "$qloc", ",", "$ztloc", ",", 
         "k\[CapitalLambda]04"}], "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
     
     RowBox[{"{", 
      RowBox[{
       RowBox[{"1", "/", 
        RowBox[{"ain", "[", 
         RowBox[{
          RowBox[{"-", "1"}], ",", 
          RowBox[{"-", "1"}], ",", "1", ",", " ", "1", ",", 
          "k\[CapitalLambda]04"}], "]"}]}], "-", "1"}], "}"}]}], "}"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.683161425037785*^9, 3.683161425946073*^9}, {
   3.683162951133165*^9, 3.683162956011346*^9}, 3.683162999848049*^9, 
   3.683163077727189*^9, {3.68330527817782*^9, 3.6833053526822033`*^9}, {
   3.683307417657935*^9, 3.683307419645567*^9}, {3.683309851010384*^9, 
   3.6833098686696873`*^9}, {3.683320599338426*^9, 3.6833206084732513`*^9}, {
   3.683379408438733*^9, 3.683379414057177*^9}, {3.683379447410449*^9, 
   3.68337946258189*^9}, {3.683382255146508*^9, 3.683382255944065*^9}, {
   3.683382417349123*^9, 3.683382436639237*^9}, {3.6833825355828323`*^9, 
   3.683382538265565*^9}, {3.683384645485897*^9, 3.6833846567234173`*^9}, {
   3.683397149988208*^9, 3.683397152299148*^9}, {3.683399810205421*^9, 
   3.6833998145005293`*^9}, {3.683399849396399*^9, 3.683399888873996*^9}, 
   3.6834032297574997`*^9, {3.683403948060717*^9, 3.683403948896758*^9}, {
   3.68340409806301*^9, 3.6834040995667048`*^9}, {3.68340450910546*^9, 
   3.68340450985048*^9}, {3.688411129470613*^9, 3.688411129843931*^9}, {
   3.688413889116188*^9, 3.6884138904826183`*^9}, 3.688414162510841*^9, {
   3.688414205792842*^9, 3.6884142215387707`*^9}, {3.688416584518701*^9, 
   3.688416630556674*^9}, {3.6884166683048897`*^9, 3.6884166853654623`*^9}, {
   3.68841672761899*^9, 3.688416779374528*^9}, {3.68841681903526*^9, 
   3.688416851606502*^9}, {3.688416903079126*^9, 3.688416945839342*^9}, {
   3.688418702069017*^9, 3.688418753226075*^9}, {3.688418786160646*^9, 
   3.6884188187732983`*^9}, {3.688418852498538*^9, 3.688418909037262*^9}, {
   3.6884189660281343`*^9, 3.6884190044603233`*^9}, {3.688420825714984*^9, 
   3.688420871764834*^9}, {3.688421309144226*^9, 3.6884213173229427`*^9}, {
   3.688912222313799*^9, 3.68891223061893*^9}, {3.688912293117421*^9, 
   3.688912323392864*^9}, {3.6889123650271673`*^9, 3.6889123672072697`*^9}, {
   3.6889125438175917`*^9, 3.688912544111505*^9}, {3.688912639820519*^9, 
   3.688912644515976*^9}, {3.68891277628225*^9, 3.68891281791791*^9}, {
   3.688912869469184*^9, 3.688912890199646*^9}, {3.7638410070427723`*^9, 
   3.763841013759933*^9}},ExpressionUUID->"f90e52a8-143c-49cc-9633-\
f04d6b1e9277"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"which", " ", "mode", " ", "is", " ", "entering", " ", 
      RowBox[{"today", "?", " ", "\[IndentingNewLine]", "1"}], "st", " ", 
      "expression"}], " ", "=", " ", 
     RowBox[{
      RowBox[{
       RowBox[{"k", "\[CenterDot]", "\[Tau]"}], 
       RowBox[{"(", "a", ")"}]}], "=", "1"}]}], ",", " ", 
    RowBox[{
     RowBox[{"2", "nd", " ", "one"}], " ", "=", " ", 
     RowBox[{
      RowBox[{
       RowBox[{"a", "\[CenterDot]", "H"}], 
       RowBox[{"(", "a", ")"}]}], "=", "k"}]}]}], "\[IndentingNewLine]", 
   "*)"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"ScientificForm", "[", 
     RowBox[{"khor", "[", 
      RowBox[{
       RowBox[{"-", "1"}], ",", " ", 
       RowBox[{"-", "1"}], ",", " ", "1", ",", " ", "1", ",", " ", "1"}], 
      "]"}], "]"}], ",", " ", 
    RowBox[{"ScientificForm", "[", 
     RowBox[{"k", "[", 
      RowBox[{
       RowBox[{"-", "1"}], ",", " ", 
       RowBox[{"-", "1"}], ",", " ", "1", ",", " ", "1", ",", " ", "1"}], 
      "]"}], "]"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.6833823698349867`*^9, 3.6833823969542418`*^9}, {
   3.683382608784711*^9, 3.6833826232018337`*^9}, {3.683403217146212*^9, 
   3.683403221990299*^9}, {3.6884109993477373`*^9, 3.688411056969543*^9}, 
   3.7516538872713633`*^9},ExpressionUUID->"931c3149-a81e-4728-8e1d-\
2002ddf71a7b"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"$kdesi", "=", 
    RowBox[{"2", "*", 
     RowBox[{"N", "[", "Pi", "]"}], "*", "3", "*", 
     RowBox[{"10", "^", 
      RowBox[{"-", "4"}]}]}]}], ";"}], " ", 
  RowBox[{"(*", 
   RowBox[{"2", 
    RowBox[{"\[Pi]", "/", "3"}], "Gpc"}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"$kbao", "=", " ", 
    RowBox[{"2", "*", 
     RowBox[{
      RowBox[{"N", "[", "Pi", "]"}], "/", "147"}]}]}], ";"}], " ", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"2", 
     RowBox[{"\[Pi]", " ", "/", "\[Lambda]"}]}], ";", " ", 
    RowBox[{
     RowBox[{"\[Lambda]", " ", "~", " ", "147"}], "Mpc"}]}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"$knl", "=", "0.1"}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"$kpivote", "=", 
    RowBox[{"3", "*", 
     RowBox[{"10", "^", 
      RowBox[{"-", "3"}]}]}]}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"<", 
    RowBox[{
     RowBox[{"--", 
      RowBox[{"-", " ", "how"}]}], " ", "to", " ", "choose", " ", "this", " ", 
     RowBox[{"value", "?"}]}]}], " ", "*)"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{"{", 
  RowBox[{"$kdesi", ",", " ", "$kbao"}], "}"}]}], "Input",
 InitializationCell->True,
 CellChangeTimes->{{3.683161425037785*^9, 3.683161425946073*^9}, {
   3.683162951133165*^9, 3.683162956011346*^9}, 3.683162999848049*^9, 
   3.683163077727189*^9, {3.68330527817782*^9, 3.6833053526822033`*^9}, {
   3.683307417657935*^9, 3.683307419645567*^9}, {3.683309851010384*^9, 
   3.6833098686696873`*^9}, {3.683320599338426*^9, 3.6833206084732513`*^9}, {
   3.683379408438733*^9, 3.683379414057177*^9}, {3.683379447410449*^9, 
   3.68337946258189*^9}, {3.683382255146508*^9, 3.683382255944065*^9}, {
   3.683382417349123*^9, 3.683382436639237*^9}, {3.6833825355828323`*^9, 
   3.683382538265565*^9}, {3.683384645485897*^9, 3.6833846567234173`*^9}, {
   3.683397149988208*^9, 3.683397152299148*^9}, {3.683399810205421*^9, 
   3.6833998145005293`*^9}, {3.683399849396399*^9, 3.683399888873996*^9}, 
   3.6834032297574997`*^9, {3.683403948060717*^9, 3.683403948896758*^9}, {
   3.68340409806301*^9, 3.6834040995667048`*^9}, {3.68340450910546*^9, 
   3.68340450985048*^9}, {3.688411129470613*^9, 3.688411129843931*^9}, {
   3.688413889116188*^9, 3.6884138904826183`*^9}, 3.688414162510841*^9, {
   3.688414205792842*^9, 3.6884142215387707`*^9}, {3.688416584518701*^9, 
   3.688416630556674*^9}, {3.6884166683048897`*^9, 3.6884166853654623`*^9}, {
   3.68841672761899*^9, 3.688416779374528*^9}, {3.68841681903526*^9, 
   3.688416851606502*^9}, {3.688416903079126*^9, 3.688416945839342*^9}, {
   3.688418702069017*^9, 3.688418753226075*^9}, {3.688418786160646*^9, 
   3.6884188187732983`*^9}, {3.688418852498538*^9, 3.688418909037262*^9}, {
   3.6884189660281343`*^9, 3.6884190044603233`*^9}, {3.688420825714984*^9, 
   3.688420871764834*^9}, {3.688421309144226*^9, 3.6884213173229427`*^9}, {
   3.688912222313799*^9, 3.68891223061893*^9}, {3.688912293117421*^9, 
   3.688912323392864*^9}, {3.6889123650271673`*^9, 3.6889123672072697`*^9}, {
   3.6889125438175917`*^9, 3.688912544111505*^9}, {3.688912639820519*^9, 
   3.688912644515976*^9}, {3.6889129221261883`*^9, 3.6889129421220922`*^9}, 
   3.688913091546151*^9, {3.688913756363472*^9, 3.6889137564823427`*^9}, {
   3.6889142703728943`*^9, 3.688914286898272*^9}, {3.6889143932443*^9, 
   3.688914393398499*^9}, {3.688915522412243*^9, 3.688915539090734*^9}, {
   3.688916411302256*^9, 3.688916418786454*^9}, {3.689370028227313*^9, 
   3.689370032399764*^9}, {3.689370106335465*^9, 3.6893701987721567`*^9}, {
   3.689370336257883*^9, 3.68937036750961*^9}, {3.714141840592993*^9, 
   3.7141418467972*^9}, 3.714154511402049*^9, 3.714155088320404*^9, 
   3.7607074825713778`*^9},ExpressionUUID->"8560f99a-b6b6-48c0-a9f9-\
93d7b81931bd"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0.0018849555921538756`", ",", "0.042742757191697865`"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.763837787143332*^9, 3.763840806643813*^9, 
  3.7663501321372843`*^9, 3.767732885751709*^9, 
  3.768908536620532*^9},ExpressionUUID->"c31671c6-bce4-4914-8919-\
4663cfeefeb7"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData["$h"], "Input",
 CellChangeTimes->{{3.768908569032773*^9, 
  3.768908570125984*^9}},ExpressionUUID->"1dc2b9a2-f40d-4b96-8106-\
07bcbea8f006"],

Cell[BoxData["0.6774`"], "Output",
 CellChangeTimes->{
  3.768908571740506*^9},ExpressionUUID->"fc114697-488d-43e1-bdde-\
c4ce9f389803"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData["$\[CapitalOmega]m0"], "Input",
 CellChangeTimes->{{3.768908573786386*^9, 
  3.768908581328063*^9}},ExpressionUUID->"744b2258-b830-4613-83c9-\
282835d2df1b"],

Cell[BoxData["0.30749398806757705`"], "Output",
 CellChangeTimes->{
  3.768908581960814*^9},ExpressionUUID->"4368bccc-31e9-415c-bfb1-\
1f6f9c0f76ce"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{735, 683},
WindowMargins->{{Automatic, 309}, {15, Automatic}},
FrontEndVersion->"11.1 para Mac OS X x86 (32-bit, 64-bit Kernel) (March 16, \
2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 340, 5, 92, "Title", "ExpressionUUID" -> \
"7f9cb2dd-5a08-48c1-ab40-46a7060cd80c"],
Cell[923, 29, 2191, 49, 349, "Input", "ExpressionUUID" -> \
"59a089bf-cd60-48f9-b4b9-61e27bd5ca16",
 InitializationCell->True],
Cell[3117, 80, 727, 19, 70, "Input", "ExpressionUUID" -> \
"d086d603-598c-4a4a-87d9-68407a9d9d49",
 InitializationCell->True],
Cell[3847, 101, 547, 15, 91, "Input", "ExpressionUUID" -> \
"45176645-0b03-4662-abd4-bf05944962c5",
 InitializationCell->True],
Cell[CellGroupData[{
Cell[4419, 120, 218, 4, 35, "Subsubsection", "ExpressionUUID" -> \
"25024269-79e1-48b0-801d-5b5106be5974"],
Cell[4640, 126, 768, 20, 96, "Input", "ExpressionUUID" -> \
"69cc7edb-cfef-487d-a844-1e75500aa1ea"],
Cell[CellGroupData[{
Cell[5433, 150, 561, 15, 54, "Input", "ExpressionUUID" -> \
"8924922d-8590-4401-939b-9ca29d4b7a03"],
Cell[5997, 167, 133, 3, 32, "Output", "ExpressionUUID" -> \
"251aabbf-7f9b-4850-8ab9-e7b46dface51"],
Cell[6133, 172, 152, 3, 32, "Output", "ExpressionUUID" -> \
"443b1752-905f-445b-8c62-f65c0ea48b94"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6322, 180, 257, 4, 46, "Input", "ExpressionUUID" -> \
"e1b39148-7a36-48f6-8cdb-13025c4000ac"],
Cell[6582, 186, 358, 11, 43, "Output", "ExpressionUUID" -> \
"6aa8a1d3-c84e-4953-af1b-f6ff8aadb03a"]
}, Open  ]],
Cell[6955, 200, 129, 3, 32, "Input", "ExpressionUUID" -> \
"a81e7e1f-47bd-438e-b5b4-fb35fcf0c680"],
Cell[CellGroupData[{
Cell[7109, 207, 466, 11, 32, "Input", "ExpressionUUID" -> \
"e5d7790c-c343-4b68-9797-09fc6daa62a2"],
Cell[7578, 220, 321, 6, 32, "Output", "ExpressionUUID" -> \
"ee3e47b6-8a72-4a8e-b47c-eec1d4a33431"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7936, 231, 189, 3, 46, "Input", "ExpressionUUID" -> \
"a5c408fe-c12d-4415-ac65-5046689f2182"],
Cell[8128, 236, 310, 10, 32, "Output", "ExpressionUUID" -> \
"2c9de5b6-6104-44d7-8107-137bb343ffcd"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8475, 251, 164, 2, 46, "Input", "ExpressionUUID" -> \
"c1c9da39-9254-4d32-8adf-f034859fa6e2"],
Cell[8642, 255, 308, 10, 32, "Output", "ExpressionUUID" -> \
"8b8a965e-247e-46af-aa1b-fe086459f529"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8987, 270, 405, 10, 75, "Input", "ExpressionUUID" -> \
"169e8819-838d-43ba-b405-1a295c2b931b"],
Cell[9395, 282, 174, 3, 32, "Output", "ExpressionUUID" -> \
"53cf257f-8d8f-49d4-b9fc-c5f0ac41145f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9606, 290, 343, 9, 54, "Input", "ExpressionUUID" -> \
"27b4f0f5-5e49-45bc-952c-e07e4057f3a8"],
Cell[9952, 301, 175, 3, 32, "Output", "ExpressionUUID" -> \
"41186dc6-a7d0-49a9-91dc-9441ec30b463"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10164, 309, 2889, 76, 300, "Input", "ExpressionUUID" -> \
"f11d361e-4ee9-4ea0-9966-fed98a311f6b"],
Cell[13056, 387, 221, 4, 32, "Output", "ExpressionUUID" -> \
"baff5b36-d2a0-4fec-9045-8ffb147ffc56"],
Cell[13280, 393, 913, 27, 35, "Output", "ExpressionUUID" -> \
"f7838265-fd42-4bac-92ca-ec24f4d1c5e6"]
}, Open  ]],
Cell[14208, 423, 4498, 100, 216, "Input", "ExpressionUUID" -> \
"58614303-03a6-4d8e-9f10-bb283a27df38"],
Cell[18709, 525, 4544, 100, 216, "Input", "ExpressionUUID" -> \
"f90e52a8-143c-49cc-9633-f04d6b1e9277"],
Cell[23256, 627, 1463, 37, 151, "Input", "ExpressionUUID" -> \
"931c3149-a81e-4728-8e1d-2002ddf71a7b"],
Cell[CellGroupData[{
Cell[24744, 668, 3888, 79, 205, "Input", "ExpressionUUID" -> \
"8560f99a-b6b6-48c0-a9f9-93d7b81931bd",
 InitializationCell->True],
Cell[28635, 749, 312, 7, 32, "Output", "ExpressionUUID" -> \
"c31671c6-bce4-4914-8919-4663cfeefeb7"]
}, Open  ]],
Cell[CellGroupData[{
Cell[28984, 761, 154, 3, 32, "Input", "ExpressionUUID" -> \
"1dc2b9a2-f40d-4b96-8106-07bcbea8f006"],
Cell[29141, 766, 136, 3, 32, "Output", "ExpressionUUID" -> \
"fc114697-488d-43e1-bdde-c4ce9f389803"]
}, Open  ]],
Cell[CellGroupData[{
Cell[29314, 774, 170, 3, 32, "Input", "ExpressionUUID" -> \
"744b2258-b830-4613-83c9-282835d2df1b"],
Cell[29487, 779, 149, 3, 32, "Output", "ExpressionUUID" -> \
"4368bccc-31e9-415c-bfb1-1f6f9c0f76ce"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

