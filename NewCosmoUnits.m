(* ::Package:: *)

(********************************************)
(*
May 2018
M Jaber
	\[Equal] COSMOLOGICAL UNITS AND CONSTANTS \[Equal]

	for a flat universe we set
	\[CapitalOmega]de0 = 1- \[CapitalOmega]m0-\[CapitalOmega]r0

	Natural units
	
	Planck TT, TE, EE + lowP + lensing + external
	external = BAO+JLA+H0
	
	or
	
	SEoS paper 
	
	$COSMO = $PLANCK by default
*)
(*******************************************)



BeginPackage["NewCosmoUnits`"]

$COSMO::usage="Either Full-Planck or SEoS values"
$h::usage="Reduced Hubble constant"
$H0::usage="Today's value of Hubble constant in Mpc"	
$H0p::usage="H0/h in Mpc"
$\[CapitalOmega]bh2::usage="baryon physical density"
$\[CapitalOmega]ch2::usage="CDM physical density"
$\[CapitalOmega]b0::usage="baryon density fraction"
$\[CapitalOmega]c0::usage="CDM density fraction"
$\[CapitalOmega]m0::usage"Matter density fraction"
$\[CapitalOmega]r0::usage="Radiation density fraction"
$\[CapitalOmega]g0::usage="Photon density fraction"
$\[CapitalOmega]de0fid::usage="\[CapitalOmega]de = 0.6844"
$\[CapitalOmega]de0free::usage="1-\[CapitalOmega]m0-\[CapitalOmega]r0"
$\[Rho]cr0::usage="Today's critical density value"
$\[Rho]m0::usage="matter density"
$\[Rho]de0::usage="DE density"
$\[Rho]r0::usage="Radiation density"
$\[Rho]c0::usage="CDM density"
$\[CapitalOmega]de0::usage="Same as $\[CapitalOmega]de0free=1-\[CapitalOmega]m0-\[CapitalOmega]r0"


$Planck;
$SEoS;
$FPlanck;




Begin["`Private`"]


	$COSMO = $Planck;
	(*$COSMO = $SEoS;*)(* GLOBAL VALUE FOR COSMOLOGY FLAG *) (*<-- aparentemente no funciona. 
						Usar BLOCK/MODULE para escoger la cosmolog\[IAcute]a
					*)
(*					
	$CosmoPLANCK;
	$CosmoFPLANCK;
	$CosmoSEOS;*)
	
	$Cspeed = 299792458.;
	$HBAR = 1*10^(-34)*(6.62606896/(2*Pi));
	$E = 1.6021773*10^(-19); (*Electron charge*)
	$GNewton = 6.6738*10^(-11);
	$Mpc = 3.085678*10^22; (*Mpc to meters*)
	MpcGyr = 3.2615638*10^-3; 
	$KBoltz = 1.3806504*10^-23;
	$8piG = 8*Pi*$GNewton;
	$MPL = ($Cspeed^2/$E)*Sqrt[($HBAR*$Cspeed)/$8piG]; (*Planck Mass*)
	$MPLred = $MPL*Sqrt[8 N[Pi]]; (* Reduced Planck Mass*)
	
	(********)
	
	$Tg0 = 2.7255; (*Today's photon temperature in Kelvin*)
	$Neff = 3.046; 
	$THETHADEC = 1.04112*10^(-2);
	$TAU = 0.066;
	$ZDRAG = 1059.68;
	$ZDEC = 1089.90;
	$\[Rho]r0 = N[(1+(7/8)$Neff (4/11)^(4/3))*Pi^(2/15)*$Tg0^4*($KBoltz*$Mpc/($Cspeed*$HBAR))^4];
	$\[CapitalOmega]g0 = 4.64512*10^(-31) / 8.53823*10^(-27); (*which units?*)
	$\[CapitalOmega]de0fid =0.6911;


	$h = If[(*$COSMO==$Planck, 0.6787, *) (*to make it consistent with CAMB P(k)*)
	$COSMO == $Planck, 0.6774, (* feb 19 2019: full Planck cosmology*)
		If[$COSMO == $SEoS, 0.7322, 0.7]] ; 
	(*Print["El valor de h es ", $h];*)
		
	$\[CapitalOmega]ch2 = If[(*$COSMO==$Planck, 0.1176474*)(* feb 19 2019: to make it consistent with CAMB P(k)*)
		$COSMO == $Planck, 0.1188, (* feb 19 2019: full Planck*)

		If[$COSMO == $SEoS, 0.1568, 0.11]];
		
	$\[CapitalOmega]bh2 = If[(*$COSMO==$Planck, 0.02251066, *)(* feb 19 2019: to make it consistent with CAMB P(k)*)
		$COSMO == $Planck,0.02230,(* feb  2019: full Planck*)
		If[$COSMO == $SEoS,0.02225, 0.022]];



	$\[CapitalOmega]nu0 = 0.001406011; (*<---difference between Omb+Omc y OmM in Planck full + ext*)

	$H0 := 1*10^5*$h/$Cspeed; (*Today's value of Hubble constant in Mpc*)
	$H0p = 1*10^5/$Cspeed ;

	$\[Rho]cr0 = 3*10^10*$h^2*N[($MPL*$E*$Mpc)^2/($Cspeed^2*$HBAR)^2];
	$\[CapitalOmega]r0 = $\[Rho]r0/$\[Rho]cr0;
	$\[CapitalOmega]b0 := $\[CapitalOmega]bh2/$h^2;
	$\[CapitalOmega]c0 := $\[CapitalOmega]ch2/$h^2;
	$\[CapitalOmega]m0 :=  $\[CapitalOmega]b0 + $\[CapitalOmega]c0 + $\[CapitalOmega]nu0;
	$\[CapitalOmega]de0free := 1-$\[CapitalOmega]m0-$\[CapitalOmega]r0;
	$\[CapitalOmega]deh2 = $\[CapitalOmega]de0free*$h^2;
	$\[CapitalOmega]de0 =1-$\[CapitalOmega]m0-$\[CapitalOmega]r0;

	$\[Rho]m0 = $\[CapitalOmega]m0 $\[Rho]cr0;
	$\[Rho]c0 = $\[CapitalOmega]c0 $\[Rho]cr0;
	$\[Rho]b0 = $\[CapitalOmega]b0 $\[Rho]cr0;
	$\[Rho]de0 = $\[CapitalOmega]de0free $\[Rho]cr0;


End[]


EndPackage[]
