(* ::Package:: *)

(* ****************************************** *)
(* 

	Sept 2016
	M Jaber 
	
	== SOUND SPEED AND FOURIER MODES ==

	This package calculates:

	- the sound speed as function
	of the state equation and a(t)
	
	- the Fourier mode k which entered the horizon at
		some epoch ak know by the relation
		k = ak*H(ak)
	- the true size of th horizon \[Tau]hor as
		\[Tau]hor(a) = \!\(
\*SubsuperscriptBox[\(\[Integral]\), 
SuperscriptBox[\(10\), \(-6\)], \(a\)]\(
\*FractionBox[\(1\), \(a'\.b2\ H\((a')\)\)]\[DifferentialD]a'\)\)

	- the associated (true) mode entering the horizon 
		at some time ak known: 
		k*\[Tau](ak) = 1

	- the time ak when certain mode k entered to the horizon
		using relation k*\[Tau](ak) = 1


	It depends on the Packages: 
	- "PlanckCosmologyFullCMB"
	- "DEnewEoS"
	
 *)


(*
>> febreuary 2017 <<
	major change: 
	linked to CosmoUnits package instead of PlanckCosmologyFullCMB
	to properly use natural units *)
(* ****************************************** *)

(*
>> Nov 2018 <<
	major change: 
	linked to NewCosmoUnits package instead of CosmoUnits
	*)
(* ****************************************** *)


BeginPackage["SoundSpeedAndFourierModewSEoS`"]



cad2a::usage = "cad2a[$w0, $wi, $q, $zt, $a] Adiabatic Sound Speed => cad2a =  w -(a w'[a])/ ( 3 (1 + w) ) "
k::usage = "k[$w0, $wi, $q, $zt, $ak] Using k = ak*H(ak)"
ain::usage = "ain[$w0, $wi, $q, $zt, $kval] Time of entrance to the horizon of mode kval using a\[CenterDot]H(a)= k" 

\[Tau]hor::usage = "\[Tau]hor[$w0, $wi, $q, $zt, $a] Comoving size of horizon at some epoch a(t)"
khor::usage = "khor[$w0, $wi, $q, $zt, $ak] Mode k which enter at ak using k*\[Tau](ak) = 1"
ak::usage = "ak[$w0, $wi, $q, $zt, $kval] Time of entrance to the horizon of mode kval using k*\[Tau] = 1"


(*

kN::usage="kN[$w0, $w1, $Nk] Fourier Mode entering the Horizon at N=log(ak/a0)"
\[Lambda]2a::usage="\[Lambda]2a[$w0, $w1, $a, $ak] Ratio (aH/k)^2 as function of a"
\[Lambda]2N::usage="\[Lambda]2N[$w0, $w1, $N, $ak] Ratio (aH/k)^2 as function of N"

*)




Begin["`Private`"]


Get["FidCosmoUnits`"](*<--- feb 2019 change this to make it consistent*)

Get["wSEoS`"](*<--- feb 2019 change this to make it consistent*)



(* adiabatic sound speed => cad2a =  w -(a w'[a])/ ( 3 (1 + w) )*)

cad2a[$w0_, $wi_, $q_, $zt_, $a_]:= If[$w0 == $wi == -1, -1,
		Module[{$at, $a0=1},
			$at= $a0/(1+$zt);
			wprime = D[waT[$w0, $wi, $q, $at, $ai], $ai] /. ($ai -> $a);
			cad = waT[$w0, $wi, $q, $at, $a] - ($a wprime) / (3 (1+waT[$w0, $wi, $q, $at, $a]));
			Return[cad]
		]
	];

k[$w0_, $wi_, $q_, $zt_, $ak_]:= $ak HubbleaT[$w0, $wi, $q, 1/(1+$zt), $ak]



\[Tau]hor[$w0_, $wi_, $q_, $zt_, $a_?NumericQ]:= 
	NIntegrate[
		1/($ap^2 HubbleaT[$w0, $wi, $q,1/(1+$zt), $ap]),
		{$ap, 0, $a}, WorkingPrecision -> MachinePrecision];

(* k*\[Tau](Subscript[a, k]) = 1 sets the condition for the mode k to enter the horizon *)
ak[$w0_, $wi_, $q_, $zt_, $kval_]:= $ak/.
	FindRoot[$kval \[Tau]hor[$w0, $wi, $q, $zt, $ak] -1 == 0, {$ak, 10^-7,10^-1}(*,
	EvaluationMonitor\[RuleDelayed] Print[$ak]*)
];


(* a\[CenterDot]H(a) = k --> find a for a given cosmology and kvalue  *)
ain[$w0_, $wi_, $q_, $zt_, $kval_]:= $ak/.
	FindRoot[$ak * HubbleaT[$w0, $wi, $q, 1/(1+$zt), $ak] -$kval == 0, {$ak, 10^-9}
	(*,
	PrecisionGoal\[Rule]10, WorkingPrecision\[Rule]MachinePrecision, MaxIterations\[Rule]200*)
	(*FindRoot[$ak * HubbleaT[$w0, $wi, $q, 1/(1+$zt), $ak] -$kval == 0, {$ak, 10^-7,1},*)
	(*EvaluationMonitor\[RuleDelayed] Print[$ak]*)
];


khor[$w0_, $wi_, $q_, $zt_, $ak_]:= 
		1/\[Tau]hor[$w0, $wi, $q, $zt, $ak];
(*kN[$w0_, $w1_, $Nk_]:=($a0 Exp[$Nk]) HN[$w0, $w1, $Nk]; *)

(*\[Lambda]2a[$w0_, $w1_, $a_, $ak_] := (($a Ha[$w0, $w1, $a])/k[$w0, $w1, $ak])^2;

\[Lambda]2N[$w0_, $w1_, $N_, $ak_] := (($a0 Exp[$N] HN[$w0, $w1, $N])/k[$w0, $w1, $ak])^2;
*)



	


End[]

EndPackage[]
